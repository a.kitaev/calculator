package calculator;

import calculator.enums.BinaryOperationType;
import calculator.enums.TokenType;
import calculator.tokens.*;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;

import static sun.nio.ch.IOStatus.EOF;

public class Lexer implements calculator.interfaces.Lexer {

    private TokenType lastTokenType = TokenType.DEFAULT;
    private Reader reader;
    private Character currentSymbol;

    public Lexer(Reader reader) {
        this.reader = reader;
    }

    @Override
    public AbstractToken getNextToken() throws IOException {
        while (true) {
            if (currentSymbol == null) {
                currentSymbol = getNextChar();
            }
            String curStr = String.valueOf(currentSymbol);

            if (curStr.matches("[ \t\n]")) {
                currentSymbol = null;
                continue;
            }

            if (curStr.matches("[()]")) {
                return getParsedBrace(currentSymbol);
            }

            if (curStr.matches("[0-9]")) {
                return getParsedNumber(currentSymbol);
            }

            if (curStr.matches("[-+*/^]")) {
                return getParsedOperation(currentSymbol);
            }

            throw new UnsupportedOperationException(currentSymbol + " unsupported operation");
        }
    }

    private AbstractToken getParsedNumber(char currentSymbol) throws IOException {
        StringBuilder number = new StringBuilder();
        String curStr = String.valueOf(currentSymbol);

        while (curStr.matches("[0-9]")) {
            try {
                number.append(curStr);
                this.currentSymbol = getNextChar();
                curStr = String.valueOf(this.currentSymbol);
            } catch (EOFException e) {
                break;
            }
        }

        lastTokenType = TokenType.NUMBER;
        return new NumberToken(Integer.parseInt(number.toString()));
    }

    private AbstractToken getParsedBrace(char currentSymbol) {
        switch (currentSymbol) {
            case '(':
                lastTokenType = TokenType.OPENING_BRACE;
                this.currentSymbol = null;
                return new BraceToken(TokenType.OPENING_BRACE);

            case ')':
                lastTokenType = TokenType.CLOSING_BRACE;
                this.currentSymbol = null;
                return new BraceToken(TokenType.CLOSING_BRACE);

            default:
                throw new UnsupportedOperationException(currentSymbol + " is Unsupported");
        }
    }

    private AbstractToken getParsedOperation(char currentSymbol) {
        switch (currentSymbol) {
            case '+':
                return makeBinaryOperationToken(BinaryOperationType.PLUS);

            case '*':
                return makeBinaryOperationToken(BinaryOperationType.MULT);

            case '/':
                return makeBinaryOperationToken(BinaryOperationType.DIV);

            case '^':
                return makeBinaryOperationToken(BinaryOperationType.POW);

            case '-':
                if (lastTokenType.equals(TokenType.NUMBER) || lastTokenType.equals(TokenType.CLOSING_BRACE)) {
                    return makeBinaryOperationToken(BinaryOperationType.MINUS);
                } else {
                    lastTokenType = TokenType.UNARY_OPERATION;
                    this.currentSymbol = null;
                    return new UnaryOperationToken();
                }

            default:
                throw new UnsupportedOperationException(currentSymbol + " is Unsupported");
        }
    }

    private BinaryOperationToken makeBinaryOperationToken(BinaryOperationType type) {
        lastTokenType = TokenType.BINARY_OPERATION;
        this.currentSymbol = null;
        return new BinaryOperationToken(type);
    }

    private char getNextChar() throws IOException {
        int res = reader.read();
        if (res != EOF) {
            return ((char) res);
        } else {
            throw new EOFException();
        }
    }
}
