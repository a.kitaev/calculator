package calculator;

import calculator.enums.BinaryOperationType;
import calculator.enums.TokenType;
import calculator.interfaces.Lexer;
import calculator.tokens.AbstractToken;
import calculator.tokens.BinaryOperationToken;
import calculator.tokens.NumberToken;

import java.io.EOFException;
import java.io.IOException;

public class Parser implements calculator.interfaces.Parser {

    private Lexer lexer;
    private AbstractToken currentToken;

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }

    @Override
    public int parse() throws IOException {
        currentToken = lexer.getNextToken();
        return parseExpression();
    }

    private int parseExpression() throws IOException {
        int term = parseTerm();
        while (isSign(currentToken)) {
            int sign = getSign(currentToken);
            currentToken = lexer.getNextToken();
            term += sign * parseTerm();
        }
        return term;
    }

    private int parseTerm() throws IOException {
        int factor = parseFactor();
        while (isTerm(currentToken)) {
            BinaryOperationType type = getTerm(currentToken);
            currentToken = lexer.getNextToken();
            if (type == BinaryOperationType.MULT) {
                factor = factor * parseFactor();
            } else {
                factor = factor / parseFactor();
            }
        }
        return factor;
    }

    private int parseFactor() throws IOException {
        int power = parsePower();
        if (isPower(currentToken)) {
            currentToken = lexer.getNextToken();
            power = ((int) Math.pow(power, parseFactor()));
        }
        return power;
    }

    private int parsePower() throws IOException {
        int unarySign = 1;
        if (currentToken.getTokenType() == TokenType.UNARY_OPERATION) {
            currentToken = lexer.getNextToken();
            unarySign = -1;
        }
        return unarySign * parseAtom();
    }

    private int parseAtom() throws IOException {
        switch (currentToken.getTokenType()) {
            case NUMBER:
                int number = ((NumberToken) currentToken).getNumber();
                currentToken = lexer.getNextToken();
                return number;

            case OPENING_BRACE:
                currentToken = lexer.getNextToken();
                int expr = parseExpression();
                if (currentToken.getTokenType() == TokenType.CLOSING_BRACE) {
                    try {
                        currentToken = lexer.getNextToken();
                    } catch (EOFException e) {
                        // ignoring
                    }
                    return expr;
                } else {
                    throw getErr();
                }

            default:
                throw getErr();
        }
    }

    /*************************************************************************/

    private boolean isTerm(AbstractToken token) {
        if (!(token instanceof BinaryOperationToken)) {
            return false;
        }
        BinaryOperationType operType = ((BinaryOperationToken) token).getOperationType();
        return operType == BinaryOperationType.MULT || operType == BinaryOperationType.DIV;
    }

    private BinaryOperationType getTerm(AbstractToken token) {
        if (!isTerm(token)) {
            throw getErr();
        }
        BinaryOperationType type = ((BinaryOperationToken) token).getOperationType();
        switch (type) {
            case MULT:
            case DIV:
                return type;
            default:
                throw getErr();
        }
    }

    private boolean isSign(AbstractToken token) {
        if (!(token instanceof BinaryOperationToken)) {
            return false;
        }
        BinaryOperationType operType = ((BinaryOperationToken) token).getOperationType();
        return operType == BinaryOperationType.MINUS || operType == BinaryOperationType.PLUS;
    }

    private int getSign(AbstractToken token) {
        if (!isSign(token)) {
            throw getErr();
        }
        BinaryOperationType type = ((BinaryOperationToken) token).getOperationType();
        if (type == BinaryOperationType.MINUS) {
            return -1;
        } else {
            return 1;
        }
    }

    private boolean isPower(AbstractToken token) {
        if (!(token instanceof BinaryOperationToken)) {
            return false;
        }
        BinaryOperationType type = ((BinaryOperationToken) token).getOperationType();
        return (type == BinaryOperationType.POW);
    }

    private UnsupportedOperationException getErr() {
        return new UnsupportedOperationException(currentToken + " unsupported operation");
    }
}
