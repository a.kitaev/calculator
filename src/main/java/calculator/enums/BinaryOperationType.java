package calculator.enums;

public enum BinaryOperationType {
    PLUS,
    MINUS,
    MULT,
    DIV,
    POW
}