package calculator.enums;

public enum TokenType {
    DEFAULT,
    NUMBER,
    BINARY_OPERATION,
    UNARY_OPERATION,
    OPENING_BRACE,
    CLOSING_BRACE
}
