package calculator.interfaces;

import calculator.tokens.AbstractToken;

import java.io.IOException;

public interface Lexer {
    public AbstractToken getNextToken() throws IOException;
}
