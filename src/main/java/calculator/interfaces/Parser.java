package calculator.interfaces;

import java.io.IOException;

public interface Parser {
    public int parse() throws IOException;
}
