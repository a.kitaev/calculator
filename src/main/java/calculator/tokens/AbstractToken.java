package calculator.tokens;

import calculator.enums.TokenType;
import lombok.Getter;

import java.util.Objects;

public abstract class AbstractToken {
    @Getter
    TokenType tokenType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractToken that = (AbstractToken) o;
        return tokenType == that.tokenType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenType);
    }

    @Override
    public String toString() {
        return "AbstractToken{" +
                "tokenType=" + tokenType +
                '}';
    }
}