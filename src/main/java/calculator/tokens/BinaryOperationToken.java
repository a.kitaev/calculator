package calculator.tokens;

import calculator.enums.BinaryOperationType;
import calculator.enums.TokenType;
import lombok.Getter;

import java.util.Objects;

public class BinaryOperationToken extends AbstractToken {
    @Getter
    private BinaryOperationType operationType;

    public BinaryOperationToken(BinaryOperationType type) {
        this.tokenType = TokenType.BINARY_OPERATION;
        this.operationType = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BinaryOperationToken that = (BinaryOperationToken) o;
        return operationType == that.operationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), operationType);
    }

    @Override
    public String toString() {
        return "BinaryOperationToken{" +
                "operationType=" + operationType +
                ", tokenType=" + tokenType +
                '}';
    }
}
