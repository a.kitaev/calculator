package calculator.tokens;

import calculator.enums.TokenType;

public class BraceToken extends AbstractToken {

    public BraceToken(TokenType type) {
        this.tokenType = type;
    }

    @Override
    public String toString() {
        return "BraceToken{" +
                "tokenType=" + tokenType +
                '}';
    }
}
