package calculator.tokens;

import calculator.enums.TokenType;
import lombok.Getter;

import java.util.Objects;

public class NumberToken extends AbstractToken {
    @Getter
    private int number;

    public NumberToken(int number) {
        this.tokenType = TokenType.NUMBER;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberToken that = (NumberToken) o;
        return number == that.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return "NumberToken{" +
                "number=" + number +
                ", tokenType=" + tokenType +
                '}';
    }
}
