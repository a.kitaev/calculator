package calculator.tokens;

import calculator.enums.TokenType;

public class UnaryOperationToken extends AbstractToken {

    public UnaryOperationToken() {
        this.tokenType = TokenType.UNARY_OPERATION;
    }

    @Override
    public String toString() {
        return "UnaryOperationToken{" +
                "tokenType=" + tokenType +
                '}';
    }
}
