import calculator.Lexer
import calculator.enums.BinaryOperationType
import calculator.enums.TokenType
import calculator.tokens.*
import org.junit.Test
import java.io.StringReader

class LexerTest {

    @Test
    fun test() {
        checkLexer("5 - 6", listOf(
                NumberToken(5),
                BinaryOperationToken(BinaryOperationType.MINUS),
                NumberToken(6)
        ))
        checkLexer("98 + (54484) - ((6548)) * 6^(4)", listOf(
                NumberToken(98),
                BinaryOperationToken(BinaryOperationType.PLUS),
                BraceToken(TokenType.OPENING_BRACE),
                NumberToken(54484),
                BraceToken(TokenType.CLOSING_BRACE),
                BinaryOperationToken(BinaryOperationType.MINUS),
                BraceToken(TokenType.OPENING_BRACE),
                BraceToken(TokenType.OPENING_BRACE),
                NumberToken(6548),
                BraceToken(TokenType.CLOSING_BRACE),
                BraceToken(TokenType.CLOSING_BRACE),
                BinaryOperationToken(BinaryOperationType.MULT),
                NumberToken(6),
                BinaryOperationToken(BinaryOperationType.POW),
                BraceToken(TokenType.OPENING_BRACE),
                NumberToken(4),
                BraceToken(TokenType.CLOSING_BRACE)
        ))
        checkLexer("1 2200 5484 9480 + -544 * 45 )))", listOf(
                NumberToken(1),
                NumberToken(2200),
                NumberToken(5484),
                NumberToken(9480),
                BinaryOperationToken(BinaryOperationType.PLUS),
                UnaryOperationToken(),
                NumberToken(544),
                BinaryOperationToken(BinaryOperationType.MULT),
                NumberToken(45),
                BraceToken(TokenType.CLOSING_BRACE),
                BraceToken(TokenType.CLOSING_BRACE),
                BraceToken(TokenType.CLOSING_BRACE)
        ))
    }

    private fun checkLexer(str: String, collection: Collection<AbstractToken>) {
        val lexer = Lexer(StringReader(str))
        collection.forEach {
            val currentToken = lexer.nextToken
            MyAssert.assertEquals(currentToken, it)
        }
    }
}
