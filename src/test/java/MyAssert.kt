import org.junit.Assert

class MyAssert {

    companion object {

        fun assertEquals(o1: Any, o2: Any) {
            val res = o1 == o2
            println("$o1 == $o2 ? $res")
            Assert.assertTrue(res)
        }
    }
}