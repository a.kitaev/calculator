import calculator.Lexer
import calculator.Parser
import org.junit.Test
import java.io.StringReader

class ParserTest {

    @Test
    fun test() {
        checkParser("5 - 3", 2)
        checkParser("-6 * 2", -12)
        checkParser("-(6 * 2)", -12)
        checkParser("2^(2*2)", 16)
        checkParser("10*2/4", 5)
        checkParser("50--6+16/2^2", 60)
        checkParser("((((50--6))+16)/2)^2", 1296)
        checkParser("((((50*9/1+0--6))+16)/2)^2", 55696)
        checkParser("-0--0--0-1", -1)
        checkParser("2^3^2", 512)
        checkParser("(2^3)^4", 4096)
    }

    private fun checkParser(str: String, answ: Int) {
        val parser = Parser(Lexer(StringReader(str)))
        val res = parser.parse()
        MyAssert.assertEquals(answ, res)
    }
}
